name := "awn"

version := "0.1"

// organization := "au.edu.mq.comp"

//  check Java version
// initialize := {
//   val required = "1.8"
//   val current  = sys.props("java.specification.version")
//   assert(current == required, s"Unsupported JDK: java.specification.version $current != $required")
// }

// Scala compiler settings

scalaVersion := "2.12.1"

javaOptions in run += "-Dlogback.configurationFile=src/test/resources/logback-test.xml"

scalacOptions :=
    Seq (
        "-deprecation",
        "-feature",
        "-sourcepath", baseDirectory.value.getAbsolutePath,
        "-unchecked",
        "-Xfatal-warnings",
        "-Xlint",
        "-Xcheckinit"
    )

// scalacOptions += "-target:jvm-1.7"

scalacOptions in (Compile, doc) ++= Seq(
    "-groups",
    "-implicits",
    "-diagrams",
    "-diagrams-dot-restart",
    "50")

testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oDF")

// testOptions in Test <+= (target in Test) map {
//   t => Tests.Argument(TestFrameworks.ScalaTest, "-u", "%s" format (t / "../shippable/testresults"))
// }

//  scoverage
// coverageMinimum := 80
// coverageFailOnMinimum := false  //  false is safer as otherwise the build breaks
// coverageHighlighting := true    //  enable highlighting of covered/non-covered
// //  exclude some package from coverage
// coverageExcludedPackages := ".*sbtrats.*"

// Interactive settings

parallelExecution in Test := true

logLevel := Level.Info

shellPrompt <<= (name, version) { (n, v) =>
     _ => n + " " + v + "> "
}

// Dependencies

libraryDependencies ++=
    Seq (
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.0.0",
        "org.bitbucket.inkytonik.kiama" %% "kiama" % "2.0.0" % "test" classifier ("tests"),
        "org.scalatest" %% "scalatest" % "3.0.0" % "test",
        "org.scalacheck" %% "scalacheck" % "1.13.4" % "test",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.5.0",
        "ch.qos.logback" % "logback-classic" % "1.1.7"
    )


resolvers ++= Seq (
    Resolver.sonatypeRepo ("releases"),
    Resolver.sonatypeRepo ("snapshots")
)

// Rats! setup

sbtRatsSettings

ratsScalaRepetitionType := Some (ListType)

ratsUseScalaOptions := true

ratsUseScalaPositions := true

ratsDefineASTClasses := true

ratsDefinePrettyPrinter := true

ratsUseKiama := 2

// ScalariForm

import scalariform.formatter.preferences._
import SbtScalariform.ScalariformKeys

scalariformSettings
//scalariformSettingsWithIt

ScalariformKeys.preferences := ScalariformKeys.preferences.value
    .setPreference (AlignSingleLineCaseStatements, true)
    .setPreference (IndentSpaces, 4)
    .setPreference (SpaceBeforeColon, true)
    .setPreference (SpaceInsideBrackets, true)
    .setPreference (SpacesAroundMultiImports, true)
    .setPreference (PreserveSpaceBeforeArguments, true)
    .setPreference (SpacesWithinPatternBinders, true)
    .setPreference (RewriteArrowSymbols, true)
    .setPreference (SpaceInsideParentheses, true)
//    .setPreference (PreserveSpaceAfterArguments, true)
