package awn

import org.bitbucket.inkytonik.kiama.util.{ TestCompilerWithConfig }
import org.scalatest._
import org.bitbucket.inkytonik.kiama.util.FileSource

import AWNPrettyPrinter.show
import AWNSyntax._
import FileParser.{ parseFile, parseString }
import scala.reflect.io._
import scala.util.Try

class ParseFileTests extends FunSuite with Matchers {
    test( "Parse simple file" ) {

        val path = "src/test/scala/resources/"

        val tryParse : Try[ AWNSpec ] = parseFile( path + "example1.awn" )
        tryParse.isSuccess shouldBe true

    }

    test( "Parse simple file and test result" ) {

        val path = "src/test/scala/resources/"

        val tryParse : Try[ AWNSpec ] = parseFile( path + "example1.awn" )
        tryParse.isSuccess shouldBe true

        // tryParse.get shouldBe AWNSpec( "2" )
    }

    test( "Parse simple file with a ProcDecl" ) {

        val path = "src/test/scala/resources/"

        val tryParse : Try[ AWNSpec ] = parseFile( path + "example2.awn" )
        tryParse.isSuccess shouldBe true

        // tryParse.get shouldBe AWNSpec( "2" )
    }

}

class ParseStringTests extends FunSuite with Matchers {
    test( "Parse simpleproc decl P1() := P2()" ) {

        val prog1 =
            """|
               | P1() := P2()
               |""".stripMargin

        val tryParse : Try[ AWNSpec ] = parseString( prog1 )
        tryParse.isSuccess shouldBe true
        tryParse.get shouldBe AWNSpec(
            List(
                Proc(
                    ProcDecl( Name( "P1" ), List() ),
                    List(),
                    ProcDecl( Name( "P2" ), List() )
                )
            )
        )
    }

    test( "Parse simpleproc decl P1() := P2(a)" ) {

        val prog1 =
            """|
               | P1() := P2(a)
               |""".stripMargin

        val tryParse : Try[ AWNSpec ] = parseString( prog1 )
        tryParse.isSuccess shouldBe true
        tryParse.get shouldBe AWNSpec(
            List(
                Proc(
                    ProcDecl( Name( "P1" ), List() ),
                    List(),
                    ProcDecl( Name( "P2" ), List( Name( "a" ) ) )
                )
            )
        )
    }

    test( "Parse simpleproc decl P1() := [[ x = 3 ]] P2(a) P2() :=  [[ x = 1 ]]  P1(a)" ) {

        val prog1 =
            """|
               | P1() :=  [[ x = 3 ]]  P2(a)
               | P2() :=  [[ x = 1 ]]  P1(a)
               |""".stripMargin

        val tryParse : Try[ AWNSpec ] = parseString( prog1 )
        println( tryParse.get )
        tryParse.isSuccess shouldBe true
        tryParse.get shouldBe AWNSpec(
            List(
                Proc(
                    ProcDecl( Name( "P1" ), List() ),
                    List( Assign( Name( "x" ), Expr( "3" ) ) ),
                    ProcDecl( Name( "P2" ), List( Name( "a" ) ) )
                ),
                Proc(
                    ProcDecl( Name( "P2" ), List() ),
                    List( Assign( Name( "x" ), Expr( "1" ) ) ),
                    ProcDecl( Name( "P1" ), List( Name( "a" ) ) )
                )
            )
        )
    }

}
