/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

package awn

object FileParser {

    import AWNSyntax._
    import scala.util.{ Failure, Success, Try }

    /**
     * Parse a DOT file
     *
     * @param   fileName    The file to be parsed
     * @return              Return If sucess a [[AWNSyntax.AWNSpec]] AST
     *                      otherwise a parse error message.
     *
     * @todo Collect the messages (formatMessages)
     */
    def parseFile( fileName : String ) : Try[ AWNSpec ] = {
        import org.bitbucket.inkytonik.kiama.util.FileSource

        val p = new AWNParser()
        p.makeast( FileSource( fileName ), p.createConfig( List() ) ) match {
            case Left( x )  ⇒ Success( x )
            case Right( m ) ⇒ Failure( new Exception( s"Parse error ${p.formatMessages( m )}" ) )
        }
    }

    /**
     * Parse a DOT file
     *
     * @param   fileName    The file to be parsed
     * @return              Return If sucess a [[AWNSyntax.AWNSpec]] AST
     *                      otherwise a parse error message.
     *
     * @todo Collect the messages (formatMessages)
     */
    def parseString( input : String ) : Try[ AWNSpec ] = {
        import org.bitbucket.inkytonik.kiama.util.StringSource

        val p = new AWNParser()
        p.makeast( StringSource( input ), p.createConfig( List() ) ) match {
            case Left( x )  ⇒ Success( x )
            case Right( m ) ⇒ Failure( new Exception( s"Parse error ${p.formatMessages( m )}" ) )
        }
    }
}

import org.bitbucket.inkytonik.kiama.util.{ CompilerBase, Config }
import org.bitbucket.inkytonik.kiama.util.FileSource
import AWNSyntax.AWNSpec

/**
 * Provide a AWN file parser
 *
 */
class AWNParser extends CompilerBase[ AWNSpec, Config ] {

    import java.io.Reader
    import org.bitbucket.inkytonik.kiama.output.PrettyPrinterTypes.Document
    import org.bitbucket.inkytonik.kiama.util.{ Config, Emitter, OutputEmitter, Source }
    import org.bitbucket.inkytonik.kiama.util.Messaging.Messages

    def createConfig( args : Seq[ String ] ) : Config =
        new Config( args )

    //  build an AST from a Source if possible
    override def makeast( source : Source, config : Config ) : Either[ AWNSpec, Messages ] = {
        val p = new AWN( source, positions )
        val pr = p.pAWNSpec( 0 )
        if ( pr.hasValue )
            Left( p.value( pr ).asInstanceOf[ AWNSpec ] )
        else
            Right( Vector( p.errorToMessage( pr.parseError ) ) )
    }

    //  not needed as only parsing is required
    def process( source : Source, ast : AWNSpec, config : Config ) {}

    def format( ast : AWNSpec ) : Document =
        AWNPrettyPrinter.format( ast, 5 )

}
