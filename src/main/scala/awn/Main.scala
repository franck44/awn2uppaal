/*
 * This file is part of Automat.
 *
 * Copyright (C) 2015-2016 Franck Cassez, Macquarie University.
 *
 * Automat is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published  by  the
 * Free Software Foundation, either version 3  of the License,  or (at your
 * option) any later version.
 *
 * Automat is distributed in the hope that it will  be useful, but  WITHOUT
 * ANY WARRANTY;  without even  the implied  warranty of MERCHANTABILITY or
 * FITNESS  FOR A  PARTICULAR PURPOSE.  See the  GNU Lesser  General Public
 * License for more details.
 *
 * You should have received a copy of the  GNU Lesser General Public License
 * along with Automat.  (See files COPYING and COPYING.LESSER.)  If not, see
 * <http://www.gnu.org/licenses/>.
 */

/**
 * Main object that contains the function to run first
 */
object Main {

    import awn.AWNPrettyPrinter.show
    import awn.AWNSyntax.AWNSpec
    import awn.FileParser.parseFile
    import scala.reflect.io._
    import scala.util.Try

    /**
     * The main method.
     */
    def main( args : Array[ String ] ) : Unit = {

        println( s"Hello Franck - Parsing and pretty printing file ${args( 0 )}" )
        val path = "src/test/scala/resources/"

        val tryParse : Try[ AWNSpec ] = parseFile( path + args( 0 ) )
        println( tryParse.get )

        println( show( tryParse.get ) )

    }
}
