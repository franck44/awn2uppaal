addSbtPlugin ("org.bitbucket.inkytonik.sbt-rats" % "sbt-rats" % "2.4.0")

addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.13.0")

addSbtPlugin ("org.scalariform" % "sbt-scalariform" % "1.5.1")

addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.5.0")
